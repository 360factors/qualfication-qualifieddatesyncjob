/**
 * 
 */
package com.softech.qualificationsync.service.predict360;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softech.qualificationsync.service.webservice.model.Predict360UserQualificationRequest;
import com.softech.qualificationsync.service.webservice.model.PredictUserQualificationResponse;
import com.softech.qualificationsync.service.webservice.model.UserQualificationVO;
import com.softech.qualificationsync.service.webservice.rest.client.RestClient;
import com.softech.qualificationsync.service.webservice.rest.endpoint.Predict360WebServiceEndPoint;
import com.softech.qualificationsync.service.webservice.rest.endpoint.header.params.WebServiceParameters;
import com.softech.qualificationsync.service.webservice.rest.endpoint.header.params.WebServiceParametersEnum;
import com.softech.qualificationsync.service.webservice.rest.endpoint.header.params.WebServiceParamtersFactory;

/**
 * @author khurram.ahmed
 * 
 */
@Service("predict360Service")
public class Predict360Service {

	private static Logger logger = Logger.getLogger(Predict360Service.class);
	@Autowired
	private WebServiceParamtersFactory webServiceParamtersFactory;
	@Autowired
	private RestClient restClient;
	private final String predictHostProp = "predict.qualification.ws.endpoint.host";
	
	/**
	 * 
	 * @return
	 */
	public Map<String, Object> getQualificationStatusFromPredict() {
		logger.debug("Start Method : getQualificationStatusFromPredict");
		JSONObject jsonObject = null;
		Map<String,Object> responseMap = new HashMap<String, Object>();
		try {
			logger.debug("getting authtoken Method: getAuthToken");
			String authToken = getAuthToken();
			logger.debug("Got Auth Token Value : [" + authToken +"]");
			Map<String,String> headerParams =	new HashMap<String,String>();
			headerParams.put("Auth-Token", authToken);
			logger.debug("Executing Rest Service to fetch the data from Predict");
			jsonObject = restClient.executePost(getServiceURL(1),Integer.parseInt(webServiceParamtersFactory.getPropertiesScanner().getPropertyValueByName("predict.qualification.ws.endpoint.port")),null,headerParams);
			logger.debug("Response : [" + jsonObject.toString() +" ]");
			if (jsonObject.has("status") && jsonObject.getInt("status")==HttpStatus.SC_OK){
				responseMap.put("dataList",dataParseForLMS(jsonObject.getJSONArray("dataList")));
				responseMap.put("authToken", authToken);
			}
			else{
				logger.error("Error : Service Failed");
				throw new RuntimeException("Error occurred while fetching the Qualification data from predict");
			}
		} catch (Exception ex) {
			logger.error(" Error occurred during fetching the qualification data from predict : [" + ex.getMessage() +" ]");
			throw new RuntimeException(ex);
		}
		return responseMap;
	}
	/**
	 * 
	 * @param predict360UserQualificationRequests
	 * @param authToken
	 * @return
	 */
	public Map<String, Object> updateQualfifcationInPredict(List<Predict360UserQualificationRequest> predict360UserQualificationRequests, String authToken){
		JSONObject jsonObject = null;
		Map<String,Object> responseMap = new HashMap<String, Object>();
		try{
			Map<String,String> headerParams =	new HashMap<String,String>();
			headerParams.put("Auth-Token", authToken);
			jsonObject = restClient.executePost(getServiceURL(2),Integer.parseInt(webServiceParamtersFactory.getPropertiesScanner().getPropertyValueByName("predict.qualification.ws.endpoint.port")),null,headerParams,predict360UserQualificationRequests,String.class);
			logger.debug("Response : [" + jsonObject.toString() +" ]");
			if (jsonObject.has("status") && jsonObject.getInt("status")==HttpStatus.SC_OK){
				responseMap.put("dataList",dataParserForPredict(jsonObject.getJSONArray("dataList")));
			}
			else{
				logger.error("Error : Service Failed");
				throw new RuntimeException("Error occurred while updating the Qualification data in predict");
			}
		}
		catch (Exception ex){
			logger.error(" Error occurred during updating the qualification data in predict : [" + ex.getMessage() +" ]");
			throw new RuntimeException(ex);
		}
		return responseMap;
	}

	/**
	 * 
	 * @return
	 */
	private String getAuthToken(){
		logger.debug("Start Method : getAuthToken" );
		JSONObject jsonObject = null;
		WebServiceParameters restPredictParameters = webServiceParamtersFactory.
				getAuthentioncationParameterObject(WebServiceParametersEnum.PredictProxy);
			Map<String,String> headerParams =	restPredictParameters.getAuthenticationParameters();
			try{
				logger.debug("Executing service to get the Auth token");
				jsonObject	= restClient.getPredictAuthToken(headerParams,Integer.parseInt(webServiceParamtersFactory.getPropertiesScanner().getPropertyValueByName("predict.qualification.ws.endpoint.port")),null,restPredictParameters.getPredictAPIKey(),getServiceURL(0));
				logger.debug(jsonObject.toString());
				if (jsonObject.has("status") && jsonObject.getInt("status")==HttpStatus.SC_OK){
					if (jsonObject.getInt("errorCode")==0){
						String authToken = jsonObject.getString("authToken");
						logger.debug("Successfully fetch the auth token");
						return authToken;
					}
				}
				
			}
			catch (Exception ex){
				logger.error("error occurred during fetching the auth token [" + ex.getMessage() +"]");
				throw new RuntimeException(ex);
			}
		return null;
	}
	/**
	 * 
	 * @param type
	 * @return
	 */
	private String getServiceURL(int type) {
		if (type ==0){
		return webServiceParamtersFactory.getPropertiesScanner()
				.getPropertyValueByName(predictHostProp)
				+ Predict360WebServiceEndPoint.GetAuthenticationToken
						.getEndPoint();
		}
		else if (type==1){
			return webServiceParamtersFactory.getPropertiesScanner()
					.getPropertyValueByName(predictHostProp)
					+ Predict360WebServiceEndPoint.GetUserQualifications
							.getEndPoint();
			}
			
			else if (type==2){
				return webServiceParamtersFactory.getPropertiesScanner()
						.getPropertyValueByName(predictHostProp)
						+ Predict360WebServiceEndPoint.GetQualificationsUpdate
								.getEndPoint();
			
			}
		
		
		return null;
	}
	
	/**
	 * 
	 * @param jsonArray
	 * @return
	 */
	private UserQualificationVO [] dataParseForLMS(JSONArray jsonArray){
		Gson g =new Gson();
		return g.fromJson(JSONArray.fromObject(jsonArray).toString(), UserQualificationVO[].class);
			
	}
	
	private List<PredictUserQualificationResponse> dataParserForPredict(JSONArray jsonArray){
		List<PredictUserQualificationResponse> predictUserQualificationResponses = new ArrayList<>();
		Iterator<String> it  = jsonArray.iterator();
		while (it.hasNext()){
			String element = it.next();
			String [] arr = element.split("|");
			PredictUserQualificationResponse predictUserQualificationResponse = new PredictUserQualificationResponse();
			predictUserQualificationResponse.setEmailId(arr[0]);
			predictUserQualificationResponse.setQualificationName(arr[1]);
			predictUserQualificationResponse.setQualificationName(arr[2]);
			predictUserQualificationResponses.add(predictUserQualificationResponse);
		}
		
	return predictUserQualificationResponses;	
	}
}
	
	


/**
 * 
 */
package com.softech.qualificationsync.service.webservice.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;



/**
 * @author khurram.ahmed
 *
 */

@PropertySources({
@PropertySource("file:predict360integration.properties"),
@PropertySource("file:lmsintegration.properties")
})
@Component
public  class PropertiesScanner {

	private static PropertiesScanner propertiesScanner = null;
	private PropertiesScanner(){
		
	}
	
	@Autowired
	private Environment env;
	
	/**
	 * 
	 * @param pName
	 * @return
	 */
	public String getPropertyValueByName(String pName){
		return env.getProperty(pName);
	}
	/**
	 * 
	 * @return
	 */
	public static PropertiesScanner getInstance(){
		if (null == propertiesScanner){
			propertiesScanner = new PropertiesScanner();
		}
		
		return propertiesScanner;
	}
	
}

/**
 * 
 */
package com.softech.qualificationsync.service.webservice.model;

import java.io.Serializable;

/**
 * @author khurram.ahmed
 *
 */

public class Predict360UserQualificationRequest extends WebServiceRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1735680836747655022L;
	
	private String userKey = null;
	private TrainingPlan [] trainingPlan = null;
	/**
	 * @return the userKey
	 */
	public String getUserKey() {
		return userKey;
	}
	/**
	 * @param userKey the userKey to set
	 */
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	/**
	 * @return the trainingPlan
	 */
	public TrainingPlan[] getTrainingPlan() {
		return trainingPlan;
	}
	/**
	 * @param trainingPlan the trainingPlan to set
	 */
	public void setTrainingPlan(TrainingPlan[] trainingPlan) {
		this.trainingPlan = trainingPlan;
	}
	
	
	
}

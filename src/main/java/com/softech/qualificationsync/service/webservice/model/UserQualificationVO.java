/**
 * 
 */
package com.softech.qualificationsync.service.webservice.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author khurram.ahmed
 *
 */
@XmlRootElement(name="userQualification")
public class UserQualificationVO {

	private String vu360UserGuid = null;
	private Long[] qualificationIds = null;
	
	
	/**
	 * @return the qualificationIds
	 */
	public Long[] getQualificationIds() {
		return qualificationIds;
	}
	/**
	 * @param qualificationIds the qualificationIds to set
	 */
	public void setQualificationIds(Long[] qualificationIds) {
		this.qualificationIds = qualificationIds;
	}
	/**
	 * @return the vu360UserGuid
	 */
	public String getVu360UserGuid() {
		return vu360UserGuid;
	}
	/**
	 * @param vu360UserGuid the vu360UserGuid to set
	 */
	public void setVu360UserGuid(String vu360UserGuid) {
		this.vu360UserGuid = vu360UserGuid;
	}
	
	
	
}

/**
 * 
 */
package com.softech.qualificationsync.service.webservice;

import java.util.List;
import java.util.Map;

import com.softech.qualificationsync.service.webservice.model.RequestForTrainingPlanStatus;

/**
 * @author khurram.ahmed
 *
 */
public interface  WebServiceInvoker {

	/**
	 * 
	 * @param qualificationRequestData
	 * @return
	 * @throws Exception
	 */
	public  Map<String,Object> getQualificationStatusFromLMS(Object [] userQualificationVOs) throws Exception;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> getQualificationStatusFromPredict() throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> updateQualfifcationInPredict(List<?> userQualificationVOs,String authToken) throws Exception;
		
	
}

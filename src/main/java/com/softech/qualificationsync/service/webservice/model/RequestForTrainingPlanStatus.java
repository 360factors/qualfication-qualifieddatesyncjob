/**
 * 
 */
package com.softech.qualificationsync.service.webservice.model;


/**
 * @author khurram.ahmed
 *
 */

public class RequestForTrainingPlanStatus extends WebServiceRequest {

	private String vu360UserGuid = null;
	private Long [] trainingPlan_Ids = null;
	
	public RequestForTrainingPlanStatus() {
		
	}
	
	/**
	 * @return the vu360UserGuid
	 */
	public String getVu360UserGuid() {
		return vu360UserGuid;
	}
	/**
	 * @param vu360UserGuid the vu360UserGuid to set
	 */
	public void setVu360UserGuid(String vu360UserGuid) {
		this.vu360UserGuid = vu360UserGuid;
	}
	/**
	 * @return the trainingPlan_Ids
	 */
	public Long[] getTrainingPlan_Ids() {
		return trainingPlan_Ids;
	}
	/**
	 * @param trainingPlan_Ids the trainingPlan_Ids to set
	 */
	public void setTrainingPlan_Ids(Long[] trainingPlan_Ids) {
		this.trainingPlan_Ids = trainingPlan_Ids;
	}
	
	
	
	
}

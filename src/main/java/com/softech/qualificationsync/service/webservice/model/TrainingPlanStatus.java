/**
 * 
 */
package com.softech.qualificationsync.service.webservice.model;

/**
 * @author khurram.ahmed
 *
 */
public enum TrainingPlanStatus {

	COMPLETED,
	NOT_COMPLETED
	
}

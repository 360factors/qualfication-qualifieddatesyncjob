/**
 * 
 */
package com.softech.qualificationsync.service.webservice.rest.endpoint.header.params;

import java.util.HashMap;
import java.util.Map;

import com.softech.qualificationsync.service.webservice.util.PropertiesScanner;

/**
 * @author khurram.ahmed
 *
 */

public class Predict360ProxyParameters extends WebServiceParameters {
	
	Predict360ProxyParameters(PropertiesScanner propertiesScanner){
		super(propertiesScanner);
		
	}
	
	public Predict360ProxyParameters() {
		super();
	}

	
	@Override
	public Map<String, String> getAuthenticationParameters() {
		Map<String,String> webHeadersParams = new HashMap<String, String>();
		webHeadersParams.put("userName", this.getPredictUserName());
		webHeadersParams.put("password", this.getPredictPassword());
		return webHeadersParams;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getAPIKey(){
		return this.getAPIKey();
	}

}

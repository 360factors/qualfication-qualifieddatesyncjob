/**
 * 
 */
package com.softech.qualificationsync.service.webservice.rest.endpoint.header.params;

import java.util.Map;

import com.softech.qualificationsync.service.webservice.util.PropertiesScanner;



/**
 * @author khurram.ahmed
 *
 */

public class WebServiceParameters {
	
	private  String distributorUserName=null;
	private  String distributorPassword="";
	private  String predictAPIKey="";
	private  String predictUserName="";
	private  String predictPassword="";
	
	
	public WebServiceParameters() {
		
	}
	
	/**
	 * 
	 */
	public WebServiceParameters(PropertiesScanner propertiesScanner) {
		
		this.distributorUserName = propertiesScanner.getPropertyValueByName("predict.lms.distributor.username");
		this.distributorPassword = propertiesScanner.getPropertyValueByName("predict.lms.distributor.password");
		this.predictAPIKey = propertiesScanner.getPropertyValueByName("predict.qualification.none.api.key");
		this.predictUserName = propertiesScanner.getPropertyValueByName("predict.qualification.admin.user");
		this.predictPassword = propertiesScanner.getPropertyValueByName("predict.qualification.admin.password");
	}

	/**
	 * 
	 * @return {@link WebServiceParameters}
	 */
	public  Map<String,String> getAuthenticationParameters(){
		
		return null;
	}

	/**
	 * @return the distributorUserName
	 */
	public final String getDistributorUserName() {
		return distributorUserName;
	}


	/**
	 * @return the distributorPassword
	 */
	public final String getDistributorPassword() {
		return distributorPassword;
	}

	/**
	 * @return the predictAPIKey
	 */
	public String getPredictAPIKey() {
		return predictAPIKey;
	}

	/**
	 * @return the predictUserName
	 */
	public String getPredictUserName() {
		return predictUserName;
	}

	/**
	 * @return the predictPassword
	 */
	public String getPredictPassword() {
		return predictPassword;
	}

	
	
	
	
	
}

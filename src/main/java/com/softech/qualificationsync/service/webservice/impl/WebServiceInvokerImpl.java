/**
 * 
 */
package com.softech.qualificationsync.service.webservice.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.qualificationsync.service.lms.TrainingPlanService;
import com.softech.qualificationsync.service.predict360.Predict360Service;
import com.softech.qualificationsync.service.webservice.WebServiceInvoker;
import com.softech.qualificationsync.service.webservice.model.Predict360UserQualificationRequest;

/**
 * @author khurram.ahmed
 *
 */
@Service("WebServiceInvoker")
public class WebServiceInvokerImpl implements WebServiceInvoker {

	@Autowired
	Predict360Service predict360Service;
	@Autowired TrainingPlanService trainingPlanService;
	
	
	/* (non-Javadoc)
	 * @see com.softect.predict.service.webservice.WebServiceInvoker#getQualificationStatus(java.util.Map)
	 */
	public Map<String, Object> getQualificationStatusFromLMS(Object [] userQualificationVOs
			) throws Exception {
		return trainingPlanService.getQualificationStatusFromLMS(userQualificationVOs);
	}

	@Override
	public Map<String, Object> getQualificationStatusFromPredict() throws Exception {
		return predict360Service.getQualificationStatusFromPredict();
	}

	@Override
	public Map<String, Object> updateQualfifcationInPredict(List<?> predict360UserQualificationRequests,String authToken ) throws Exception {
		return predict360Service.updateQualfifcationInPredict((List<Predict360UserQualificationRequest>) predict360UserQualificationRequests, authToken);
	}
}

package com.softech.qualificationsync.service.webservice.rest.client;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.internal.StringMap;

@Component
public class RestClient {
	private String clazzNmae = null;
	private static Logger logger = Logger.getLogger(RestClient.class);
	
	public JSONObject executePost(String endPointUrl, int port, String TLSProtocol,
			Map<String, String> headerParameters, Object object,Class c)
			throws Exception {

		HttpPost postRequest = new HttpPost(endPointUrl);
		// Add additional header to getRequest which accepts application/xml
		// data
		addRequestHeader(postRequest, headerParameters);
		JSONObject jsonObject = new JSONObject();
		Gson g =new Gson();
		String jsonString ="";
		if (c !=null && !c.getSimpleName().equalsIgnoreCase("string")){
			jsonObject.put(c.getSimpleName(), g.toJson(object));
			jsonString = JSONObject.fromObject(jsonObject).toString();
		}
		else{
			clazzNmae = c.getSimpleName();
			jsonString = g.toJson(object).toString();
		}
		
		StringEntity entity = new StringEntity(jsonString, "UTF-8");
		BasicHeader basicHeader = new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json");
		entity.setContentType(basicHeader);
		postRequest.setEntity(entity);

		return execute(postRequest, port, TLSProtocol, 2);
	}

	public JSONObject executePost(String endPointUrl, int port, String TLSProtocol,
			Map<String, String> headerParameters) throws Exception {

		HttpPost postRequest = new HttpPost(endPointUrl);
		// Add additional header to getRequest which accepts application/xml
		// data
		addRequestHeader(postRequest, headerParameters);
		StringEntity entity = new StringEntity("", "UTF-8");

		BasicHeader basicHeader = new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json");
		entity.setContentType(basicHeader);
		postRequest.setEntity(entity);
		return execute(postRequest, port, TLSProtocol, 3);

	}

	public JSONObject executePost(String endPointUrl, int port, String TLSProtocol,
			Map<String, String> headerParameters, String jsonString)
			throws Exception {

		HttpPost postRequest = new HttpPost(endPointUrl);
		// Add additional header to getRequest which accepts application/xml
		// data
		addRequestHeader(postRequest, headerParameters);

		StringEntity entity = new StringEntity(jsonString, "UTF-8");
		BasicHeader basicHeader = new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json");
		entity.setContentType(basicHeader);
		postRequest.setEntity(entity);

		return execute(postRequest, port, TLSProtocol, 2);
	}

	public JSONObject getPredictAuthToken(Map<String, String> params, int port, String TLSProtocol,
			String predictAdminApiKey, String endPointUrl) throws Exception {
		logger.debug(endPointUrl);
		HttpPost postRequest = new HttpPost(endPointUrl);
		postRequest.addHeader("Accept", "application/json");
		postRequest.addHeader("api-key", predictAdminApiKey);
		Gson json = new Gson();
		String POST_JSON = json.toJson(params);
		StringEntity entity = new StringEntity(POST_JSON, "UTF-8");
		BasicHeader basicHeader = new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json");
		entity.setContentType(basicHeader);
		postRequest.setEntity(entity);

		return execute(postRequest, port, TLSProtocol, 1);
	}

	private JSONObject execute(HttpPost postRequest, int port, String TLSProtocol, int executeType) throws Exception {

		String restResponse = null;
		JSONObject jsonObj = null;
		HttpClient httpClient = getProtocolSpecificClient(postRequest.getURI().toString(), port, TLSProtocol);
		// Send the request; It will immediately return the response in
		// HttpResponse object
		HttpResponse response = httpClient.execute(postRequest);
		// verify the valid error code first
		int httpStatusCode = response.getStatusLine().getStatusCode();
		// Now pull back the response object
		HttpEntity httpEntity = response.getEntity();
		restResponse = EntityUtils.toString(httpEntity);
		if (executeType ==1){
			HashMap<String, Object> hm = new HashMap<String, Object>();
			Gson json = new Gson();
			hm = json.fromJson(restResponse, HashMap.class);
			List outputArr = (List) hm.get("output");
			StringMap<String> reponseParam = (StringMap<String>) outputArr.get(0);
			jsonObj = new JSONObject();
			jsonObj.put("authToken", reponseParam.get("authToken"));
			jsonObj.put("errorCode", hm.get("errorCode"));
		}
		else if (executeType == 2){
			if (null != clazzNmae && clazzNmae.equalsIgnoreCase("string")){
				Gson gson = new Gson();
				jsonObj = new JSONObject();
				jsonObj.put("dataList", restResponse);
			}
			else{
				 jsonObj = JSONObject.fromObject(restResponse);
			}
			
			
		}
		else if (executeType ==3){
			JSONArray jsonArray = JSONArray.fromObject(restResponse);
			jsonObj = new JSONObject();
			jsonObj.put("dataList",jsonArray);
		}
		
		jsonObj.put("status", httpStatusCode);

		return jsonObj;
	}

	private JSONObject executeToken(HttpPost postRequest) throws Exception {

		String restResponse = null;

		HttpClient httpClient = new DefaultHttpClient();
		// Send the request; It will immediately return the response in
		// HttpResponse object
		HttpResponse response = httpClient.execute(postRequest);
		// verify the valid error code first
		int httpStatusCode = response.getStatusLine().getStatusCode();
		// Now pull back the response object
		HttpEntity httpEntity = response.getEntity();
		restResponse = EntityUtils.toString(httpEntity);
		HashMap<String, Object> hm = new HashMap<String, Object>();
		Gson json = new Gson();
		hm = json.fromJson(restResponse, HashMap.class);
		List outputArr = (List) hm.get("output");
		StringMap<String> reponseParam = (StringMap<String>) outputArr.get(0);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("authToken", reponseParam.get("authToken"));
		jsonObj.put("errorCode", hm.get("errorCode"));
		jsonObj.put("status", httpStatusCode);

		return jsonObj;
	}

	private void addRequestHeader(HttpPost postRequest,
			Map<String, String> headerParameters) {

		postRequest.addHeader("Accept", "application/json");
		if (headerParameters != null && headerParameters.size() > 0) {
			for (String key : headerParameters.keySet()) {

				postRequest.addHeader(key, headerParameters.get(key));
			}
		}
	}
	
	/**
	 * Returns httpClient based on HTTP/HTTPS protocols.
	 * @param URI - Request URI as a String
	 * @param port - Communication port
	 * @param TLSProtocol - TLS protocol to use for SSL based request
	 * @return {@link HttpClient}
	 * @throws Exception
	 * @author muhammad.fahad
	 */
	private HttpClient getProtocolSpecificClient(String URI, int port, String TLSProtocol) throws Exception {
		HttpClient httpClient = null;
		if(URI.toLowerCase().contains("https") && TLSProtocol != null) {
			SSLContext sslContext = SSLContext.getInstance(TLSProtocol);
	        sslContext.init(null, null, new SecureRandom());
	        SSLSocketFactory sf = new SSLSocketFactory(sslContext);
	        Scheme httpsScheme = new Scheme("https", sf, port);
	        SchemeRegistry schemeRegistry = new SchemeRegistry();
	        schemeRegistry.register(httpsScheme);
	        ClientConnectionManager cm =  new SingleClientConnManager(null, schemeRegistry);
			httpClient = new DefaultHttpClient(cm, null);
		} else {
			httpClient = new DefaultHttpClient();
		}
		return httpClient;
	}

}

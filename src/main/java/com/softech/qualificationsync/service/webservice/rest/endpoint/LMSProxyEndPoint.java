package com.softech.qualificationsync.service.webservice.rest.endpoint;



// LMS proxy end points 
public enum LMSProxyEndPoint {
	
	GetQualificationStatus("/LS360ProxyAPIWeb/restful/lms/customer/trainingplan/hasCoursesCompleted"),
	GetQualificationCoursesStatus ("/LS360ProxyAPIWeb/restful/lms/customer/trainingplan/hasCoursesCompleted");
	private String endPoint = "";
	
	private LMSProxyEndPoint(String endPoint) {
	
		this.endPoint = 
				 endPoint; 
	}
	
	public String getEndPoint() {
		
		return endPoint;
	}
}	
	

/**
 * 
 */
package com.softech.qualificationsync.service.webservice.rest.endpoint.header.params;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.qualificationsync.service.webservice.util.PropertiesScanner;

/**
 * @author khurram.ahmed
 *Factory class to return the appropriate web service parameter object
 */

@Service("webServiceParamtersFactory")
public class WebServiceParamtersFactory {
	@Autowired
	private PropertiesScanner propertiesScanner;

	/**
	 * 
	 * @param webServiceParametersEnum
	 * @return {@link WebServiceParameters}
	 */
	public WebServiceParameters getAuthentioncationParameterObject(WebServiceParametersEnum webServiceParametersEnum){
		WebServiceParameters webServiceParameters = null;
		switch (webServiceParametersEnum) {
		
		case RESTProxy:
			webServiceParameters = new RestProxyParameters(propertiesScanner);
			break;
			
		case PredictProxy:
			webServiceParameters = new Predict360ProxyParameters(propertiesScanner);
			break;
		default:
			break;
		}
		
		
		return webServiceParameters;
	}
	
	public PropertiesScanner getPropertiesScanner(){
		return this.propertiesScanner;
	}
}

/**
 * 
 */
package com.softech.qualificationsync.service.webservice.rest.endpoint.header.params;


/**
 * @author khurram.ahmed
 *
 */

public enum WebServiceParametersEnum {
	RESTProxy,
	PredictProxy;
	
}

/**
 * 
 */
package com.softech.qualificationsync.service.webservice.model;

import java.util.Date;

/**
 * @author khurram.ahmed
 *
 */
public class TrainingPlan {

	private Long trainingPlanId = 0L;
	private String trainingPlanName = null;
	private TrainingPlanStatus trainingPlanStatus;
	private String completationDate = null;
	/**
	 * @return the trainingPlanId
	 */
	public Long getTrainingPlanId() {
		return trainingPlanId;
	}
	/**
	 * @param trainingPlanId the trainingPlanId to set
	 */
	public void setTrainingPlanId(Long trainingPlanId) {
		this.trainingPlanId = trainingPlanId;
	}
	/**
	 * @return the trainingPlanName
	 */
	public String getTrainingPlanName() {
		return trainingPlanName;
	}
	/**
	 * @param trainingPlanName the trainingPlanName to set
	 */
	public void setTrainingPlanName(String trainingPlanName) {
		this.trainingPlanName = trainingPlanName;
	}
	/**
	 * @return the trainingPlanStatus
	 */
	public TrainingPlanStatus getTrainingPlanStatus() {
		return trainingPlanStatus;
	}
	/**
	 * @param trainingPlanStatus the trainingPlanStatus to set
	 */
	public void setTrainingPlanStatus(TrainingPlanStatus trainingPlanStatus) {
		this.trainingPlanStatus = trainingPlanStatus;
	}
	/**
	 * @return the completationDate
	 */
	public String getCompletationDate() {
		return completationDate;
	}
	/**
	 * @param completationDate the completationDate to set
	 */
	public void setCompletationDate(String completationDate) {
		this.completationDate = completationDate;
	}
	
	
	
	
	
}

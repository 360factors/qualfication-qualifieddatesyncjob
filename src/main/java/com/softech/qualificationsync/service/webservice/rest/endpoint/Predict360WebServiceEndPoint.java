package com.softech.qualificationsync.service.webservice.rest.endpoint;



// predict360 proxy end points 
public enum Predict360WebServiceEndPoint {
	
	
	GetUserQualifications("/predict360/ws/restapi/qualification/getuserqualifications"),
	GetAuthenticationToken ("/predict360/ws/restapi/auth/startSession"),
	GetQualificationsUpdate ("/predict360/ws/restapi/qualification/updateuserqualifications");
	
	
	private String endPoint = "";
	private Predict360WebServiceEndPoint(String endPoint) {
		this.endPoint = 
				 endPoint; 
	}
	
	public String getEndPoint() {
		
		return endPoint;
	}
}	
	

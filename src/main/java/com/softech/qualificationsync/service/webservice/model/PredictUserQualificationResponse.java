/**
 * 
 */
package com.softech.qualificationsync.service.webservice.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author khurram.ahmed
 *
 */

public class PredictUserQualificationResponse {

	private String userName = null;
	private String emailId= null;
	private String qualificationName = null;
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the qualificationName
	 */
	public String getQualificationName() {
		return qualificationName;
	}
	/**
	 * @param qualificationName the qualificationName to set
	 */
	public void setQualificationName(String qualificationName) {
		this.qualificationName = qualificationName;
	}
	
	
}

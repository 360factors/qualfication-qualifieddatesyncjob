/**
 * 
 */
package com.softech.qualificationsync.service.webservice.rest.endpoint.header.params;

import java.util.HashMap;
import java.util.Map;

import com.softech.qualificationsync.service.webservice.util.PropertiesScanner;



/**
 * @author khurram.ahmed
 *
 */

public class RestProxyParameters extends WebServiceParameters{
   
	private String customerKey=null;
	
	
	public RestProxyParameters(PropertiesScanner propertiesScanner){
		super(propertiesScanner);
		this.customerKey = propertiesScanner.getPropertyValueByName("predict.lms.distributor.key");
	}
	
	/**
	 * 
	 */
	public RestProxyParameters() {
		super();
	
		
	}
	
	/**
	 * 
	 */
	public RestProxyParameters(String customerKey) {
		super();
		this.customerKey = customerKey;
	}

	

	@Override
	public Map<String, String> getAuthenticationParameters() {
		// TODO Auto-generated method stub
		Map<String,String> webHeadersParams = new HashMap<String, String>();
		webHeadersParams.put("Authorization", "Basic " + this.getDistributorUserName()+":" + this.getDistributorPassword());
		webHeadersParams.put("Key", this.customerKey);
	
		return webHeadersParams;
	}
	
	

}

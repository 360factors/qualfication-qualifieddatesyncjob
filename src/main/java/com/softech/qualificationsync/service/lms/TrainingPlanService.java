/**
 * 
 */
package com.softech.qualificationsync.service.lms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.qualificationsync.service.webservice.model.Predict360UserQualificationRequest;
import com.softech.qualificationsync.service.webservice.model.RequestForTrainingPlanStatus;
import com.softech.qualificationsync.service.webservice.model.TrainingPlan;
import com.softech.qualificationsync.service.webservice.rest.client.RestClient;
import com.softech.qualificationsync.service.webservice.rest.endpoint.LMSProxyEndPoint;
import com.softech.qualificationsync.service.webservice.rest.endpoint.header.params.WebServiceParameters;
import com.softech.qualificationsync.service.webservice.rest.endpoint.header.params.WebServiceParametersEnum;
import com.softech.qualificationsync.service.webservice.rest.endpoint.header.params.WebServiceParamtersFactory;
import com.softech.qualificationsync.service.webservice.util.PropertiesScanner;


/**
 * @author khurram.ahmed
 *
 */
@Service("trainingPlanService")
public class TrainingPlanService {

	private static Logger logger = Logger.getLogger(TrainingPlanService.class);
	@Autowired
	WebServiceParamtersFactory webServiceParamtersFactory;
	@Autowired
	RestClient restClient;
	public Map<String,Object> getQualificationStatusFromLMS(Object[] userQualifications) throws Exception
	{
		logger.debug("Start Method : getQualificationStatusFromLMS : parameters length ( Qualification ) =[" + userQualifications.length+ "]");
		Map<String,Object> response = new HashMap<String,Object>();
		JSONObject jsonObject = null;
		WebServiceParameters restLmsProxyParameters = webServiceParamtersFactory.
				getAuthentioncationParameterObject(WebServiceParametersEnum.RESTProxy);
		Map<String,String> headerParams =	restLmsProxyParameters.getAuthenticationParameters();
		try{
			PropertiesScanner propertiesScanner = webServiceParamtersFactory.getPropertiesScanner();
			String lmsURL = propertiesScanner.getPropertyValueByName( "predict.lms.proxy.ws.endpoint.host")+LMSProxyEndPoint.GetQualificationStatus.getEndPoint();
			logger.debug("Executing LMS Service to fetch the qualification qualified data");
			jsonObject =  restClient.executePost(lmsURL,Integer.parseInt(propertiesScanner.getPropertyValueByName("predict.lms.proxy.ws.endpoint.port")),propertiesScanner.getPropertyValueByName("predict.lms.proxy.ws.endpoint.tlsprotocol"),headerParams,userQualifications,RequestForTrainingPlanStatus.class);
			
			if (jsonObject.has("status") && jsonObject.getInt("status")==HttpStatus.SC_OK){
				if (jsonObject.has("errors")){
					throw new RuntimeException("Error ocurred during web service method invocation :error code =["+jsonObject.getJSONArray("errors").getJSONObject(0).getInt("status")+"]:"
							+ "error Message= ["+jsonObject.getJSONArray("errors").getJSONObject(0).getString("message")+"]");
				}
				logger.debug("Successfully executed the LMS service and the data :[" + jsonObject.toString()+"]");
				response = parseDataForPredict(jsonObject);
			}
		
		}
		catch (Exception ex){
		   ex.printStackTrace();
		   throw new RuntimeException(ex);
		}
		return response;
	}
	/**
	 * 
	 * @param jsonObject
	 * @return
	 */
	private Map<String,Object> parseDataForPredict(JSONObject jsonObject) throws Exception{
		
		Map<String,Object> response = new HashMap<>();
		List<Predict360UserQualificationRequest> predict360UserQualificationRequests = new ArrayList<>();
		if (jsonObject.has("responseForTrainingPlanStatus")){
			JSONArray jsonArray = jsonObject.getJSONArray("responseForTrainingPlanStatus");
			Iterator<JSONObject> it = jsonArray.iterator();
			
			while (it.hasNext()){
				
				Predict360UserQualificationRequest predict360UserQualificationRequest = new Predict360UserQualificationRequest();
				JSONObject jsonObj = it.next();
				JSONArray  trainingPlanArray = jsonObj.getJSONArray("assignedTrainingPlans"); 
				Iterator<JSONObject> itTrainingPlans = trainingPlanArray.iterator();
				List<TrainingPlan> trainingPlans = new ArrayList<>();
				while (itTrainingPlans.hasNext()){
					
					TrainingPlan userQualificationVO = new TrainingPlan();
					JSONObject jsonTrainingPlanObj  = itTrainingPlans.next();
					if (Boolean.valueOf(jsonTrainingPlanObj.getString("overAllTrainingPlanStatus"))==Boolean.TRUE){
						if (jsonTrainingPlanObj.getString("lastCompletionDate") != null 
								&& !jsonTrainingPlanObj.getString("lastCompletionDate").trim().equals("")
								&& !jsonTrainingPlanObj.getString("lastCompletionDate").equalsIgnoreCase("NULL")){
							
							userQualificationVO.setCompletationDate(jsonTrainingPlanObj.getString("lastCompletionDate"));
							userQualificationVO.setTrainingPlanId(jsonTrainingPlanObj.getLong("trainingPlan_Id"));
							trainingPlans.add(userQualificationVO);
						}
					}
					else{
						continue;
					}
				}
				if (trainingPlans.size() > 0){
					predict360UserQualificationRequest.setUserKey(jsonObj.getString("vu360UserGuid"));
					predict360UserQualificationRequest.setTrainingPlan(trainingPlans.toArray(new TrainingPlan[trainingPlans.size()]));
				    predict360UserQualificationRequests.add(predict360UserQualificationRequest);	
				}
			}
		}
		
		response.put("dataList", predict360UserQualificationRequests);
		return response;
	}
}


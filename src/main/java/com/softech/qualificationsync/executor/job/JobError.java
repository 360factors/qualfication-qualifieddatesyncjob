/**
 * 
 */
package com.softech.qualificationsync.executor.job;

/**
 * @author khurram.ahmed
 *
 */
public class JobError {

	private String errorMsg = null;
	private JobStep jobStep = null;
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/**
	 * @return the jobStep
	 */
	public JobStep getJobStep() {
		return jobStep;
	}
	/**
	 * @param jobStep the jobStep to set
	 */
	public void setJobStep(JobStep jobStep) {
		this.jobStep = jobStep;
	}
	
	
	
}

/**
 * 
 */
package com.softech.qualificationsync.executor.job;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.softech.qualificationsync.service.webservice.WebServiceInvoker;
import com.softech.qualificationsync.service.webservice.model.UserQualificationVO;

/**
 * @author khurram.ahmed
 * 
 */
@Component
public class Predict360JobExecutor {

	@Autowired
	private WebServiceInvoker webServiceInvoker;
	private String authToken = null;
	private JobResponse jobResponse = JobResponse.getInstance();
	private  Map<String, Object> getQualificationStatusFromPredict() throws Exception {
		Map<String, Object> userQMap = new HashMap<String, Object>();
		try {
			userQMap = webServiceInvoker.getQualificationStatusFromPredict();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		return userQMap;
	}
	
	private Map<String, Object> updateQualificationsInPredict() throws Exception{
		Map<String, Object> userQMap = new HashMap<String, Object>();
		try{
			JobResponse jobResponse = JobResponse.getInstance();
			List<?> predict360UserQualificationRequests = (List<?>) jobResponse.getJobResponseMap().get(JobStep.LMS_QUALFICATION_FETCH_STEP);
			userQMap =	webServiceInvoker.updateQualfifcationInPredict(predict360UserQualificationRequests, this.authToken);
		}
		
		catch (Exception ex){
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		
		return userQMap;
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void startPredictJob(JobType jobType) throws Exception{
		Map<String,Object> responseMap = null;
	
		try {
			switch (jobType) {
				case PREDICT360_QUALIFICATION_FETCH_JOB:
					responseMap = getQualificationStatusFromPredict();
					decorateJobResponseObject(responseMap, JobStep.PREDICT_QUALIFICATION_FETCH_STEP);
				break;
				
				case PREDICT360_QUALIFICATION_UPDATE_JOB:
					responseMap = updateQualificationsInPredict();
					decorateJobResponseObject(responseMap, JobStep.PREDICT_QUALIFICATION_UPDATE_STEP);
				break;

		default:
			break;
		}
		//	throw new RuntimeException("No Data is available");
		} catch (Exception e) {
			JobStep jobStep = jobType == JobType.PREDICT360_QUALIFICATION_FETCH_JOB?JobStep.PREDICT_QUALIFICATION_FETCH_STEP:JobStep.PREDICT_QUALIFICATION_UPDATE_STEP;
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			jobResponse.getJobStepMessage().put(jobStep, sw.toString());
			jobResponse.getJobStepStatus().put(jobStep, JobStatus.FAILED);
			throw e;
		}
	
	}
	
	private void decorateJobResponseObject(	Map<String,Object> responseMap,JobStep jobStep){
		if (responseMap.size() > 0 ){
			if (responseMap.containsKey("authToken")){
				this.authToken = String.valueOf(responseMap.get("authToken"));
				responseMap.remove("authToken");
			}
			jobResponse.getJobResponseMap().put(jobStep,responseMap.get("dataList"));
			jobResponse.getJobStepMessage().put(jobStep, "true"); 
		}
		else{
			jobResponse.getJobStepMessage().put(jobStep, "false"); 
		}
			jobResponse.getJobStepStatus().put(jobStep, JobStatus.SUCCESS);
	}
}

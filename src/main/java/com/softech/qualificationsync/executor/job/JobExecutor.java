/**
 * 
 */
package com.softech.qualificationsync.executor.job;

/**
 * @author khurram.ahmed
 *
 */
public interface JobExecutor {
	public String startJobs();
	
	
}

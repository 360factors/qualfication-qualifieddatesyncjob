/**
 * 
 */
package com.softech.qualificationsync.executor.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.softech.qualificationsync.executor.job.exception.JobException;

/**
 * @author khurram.ahmed
 * 
 */
@Component
public class JobExecutorImpl implements JobExecutor {

	private static final Logger LOGGER = Logger.getLogger(JobExecutorImpl.class);
	
	@Autowired
	Predict360JobExecutor predict360JobExecutor;
	@Autowired
	LMSJobExecutor lmsJobExecutor;
	

	public JobExecutorImpl() {
	
	}

	public void jobStart(JobStep jobStep) throws JobException {
		
		try {
			switch (jobStep) {
			case PREDICT_QUALIFICATION_FETCH_STEP:
				predict360JobExecutor
						.startPredictJob(JobType.PREDICT360_QUALIFICATION_FETCH_JOB);

				break;
				
			case LMS_QUALFICATION_FETCH_STEP:
				 lmsJobExecutor.startLMSJob();
				break;
				
			case PREDICT_QUALIFICATION_UPDATE_STEP:
				 predict360JobExecutor.startPredictJob(JobType.PREDICT360_QUALIFICATION_UPDATE_JOB);
				break;

			default:
				break;
			}
		} catch (Exception e) {
				LOGGER.error("Error occured : Step failing = ["+ jobStep.name() +"]");
				throw new RuntimeException(JobException.createJobExceptin(new JobError(), "Qualification Sync Job is failed and the error is= "
						+ "[" +e.getMessage()+"]",  jobStep));
			}
	
		JobResponse jobResponse =JobResponse.getInstance();
		if (!jobResponse.getJobStepMessage().get(jobStep).toString().equalsIgnoreCase("true")){
			throw new RuntimeException("No data available to execute.");
		}
		

	}


	@Override
	public String startJobs()  {
		LOGGER.debug("Start Method : startJobs");
		try {
			// Step Job 1 --Start Get User Qualification from Predict
			LOGGER.debug("Excecuting first Job step : PREDICT_QUALIFICATION_FETCH_STEP");
			this.jobStart(JobStep.PREDICT_QUALIFICATION_FETCH_STEP);
			LOGGER.debug("Successfully executed the first Job Step : PREDICT_QUALIFICATION_FETCH_STEP");
			// Step Job 2 --Start Get User Qualification from LMS
			LOGGER.debug("Excecuting Second Job step : LMS_QUALFICATION_FETCH_STEP");
			this.jobStart(JobStep.LMS_QUALFICATION_FETCH_STEP);
			LOGGER.debug("Successfully executed the second Job Step : LMS_QUALFICATION_FETCH_STEP");
			// Step Job 3 --Start update User Qualification in predict
			LOGGER.debug("Excecuting Third Job step :PREDICT360_QUALIFICATION_UPDATE_JOB");
			this.jobStart(JobStep.PREDICT_QUALIFICATION_UPDATE_STEP);
			LOGGER.debug("Successfully executed the Third Job Step : PREDICT360_QUALIFICATION_UPDATE_JOB");
			
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.error(ex.getMessage());
			return JobStatus.FAILED.toString();
		}
		return JobStatus.SUCCESS.toString();
	}
	
	
}

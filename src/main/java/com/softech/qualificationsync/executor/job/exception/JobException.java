/**
 * 
 */
package com.softech.qualificationsync.executor.job.exception;

import com.softech.qualificationsync.executor.job.JobError;
import com.softech.qualificationsync.executor.job.JobStep;

/**
 * @author khurram.ahmed
 *
 */
public class JobException extends Exception {

	public JobException(JobError jobError){
		super();
		this.jobError = jobError;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JobError jobError = null;

	/**
	 * @return the jobError
	 */
	public JobError getJobError() {
		return jobError;
	}

	/**
	 * @param jobError the jobError to set
	 */
	public void setJobError(JobError jobError) {
		this.jobError = jobError;
	}
	
	public static JobException createJobExceptin(JobError jobError, String errorMsg , JobStep jobStep){
		
		if (jobError == null){
			jobError = new JobError();
		}
		jobError.setErrorMsg(errorMsg);
		jobError.setJobStep(jobStep);
		JobException jobException = new JobException(jobError);
		return jobException;
	}
		
}

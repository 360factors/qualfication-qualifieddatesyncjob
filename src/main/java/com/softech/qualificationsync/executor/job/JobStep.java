/**
 * 
 */
package com.softech.qualificationsync.executor.job;

/**
 * @author khurram.ahmed
 *
 */
public enum JobStep {

	PREDICT_QUALIFICATION_FETCH_STEP,
	PREDICT_QUALIFICATION_UPDATE_STEP,
	LMS_QUALFICATION_FETCH_STEP;
}

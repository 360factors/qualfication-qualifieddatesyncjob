/**
 * 
 */
package com.softech.qualificationsync.executor.job.email;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.softech.qualificationsync.config.ConfigProperties;
import com.softech.qualificationsync.executor.job.JobExecutorImpl;
import com.softech.qualificationsync.executor.job.JobResponse;

/**
 * @author khurram.ahmed
 *
 */
@Component
public class JobEmailImpl implements JobEmail {

	private static final Logger log = Logger.getLogger(JobEmail.class);
	private final String mailFromAddress = ConfigProperties.getProperties().getProperty("predict.email.fromAddress");
	private final String mailToAddress = ConfigProperties.getProperties().getProperty("mail.smtp.to");
	private final String mailFromAddressName = ConfigProperties.getProperties().getProperty("predict.email.fromAddressName");
	private final String emailSubject = "Predict User Qualification Sync Job";
	
	
	/* (non-Javadoc)
	 * @see com.softech.qualificationsync.executor.job.email.JobEmail#sendEmail()
	 */
	@Override
	public boolean sendEmail() {
	   
	    Session session = Session.getDefaultInstance( ConfigProperties.getProperties(),new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication( ConfigProperties.getProperties().getProperty("mail.smtp.user"), ConfigProperties.getProperties().getProperty("mail.smtp.password"));
            }
        });
		session.setDebug(true);
		try{
			
			MimeMessage msg = new MimeMessage(session);

			InternetAddress address = new InternetAddress(this.mailFromAddress);
			if (this.mailFromAddressName != null) {
				try {
					address.setPersonal(mailFromAddressName);
				} catch (java.io.UnsupportedEncodingException uee) {
					log.error("Could not set Personal Name:" + mailFromAddressName + uee);
				}
			}
			msg.setFrom(address);

			InternetAddress toAddresses = new InternetAddress();
			toAddresses.setAddress(mailToAddress);
			msg.setRecipient(Message.RecipientType.TO, toAddresses);

		

			msg.setSubject(emailSubject +" - " +ConfigProperties.getProperties().getProperty("running.environment"));
			msg.setSentDate(new Date());

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			mbp1.setContent(emailBody(), "text/html");

			// create the Multipart and its parts to it
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			// add the Multipart to the message
			msg.setContent(mp);

			Transport.send(msg);
		} catch (MessagingException mex) {
			log.error("Exception occurred during send of email message " + mex);
			Exception ex = null;
			if ((ex = mex.getNextException()) != null) {
				log.error("NestedException: " + ex);
				ex.printStackTrace();
			}
			return false;
		}
		return true;
	   
	
	}
	
	
	
	private String emailBody(){
		StringBuilder sb  = new StringBuilder();
		JobResponse jobResponse = JobResponse.getInstance();
		String jobStatus = jobResponse.getJobStatus();
		Map<String, String []> dataMap = jobResponse.getJobStepMetaData();
		
		sb.append("<div>Qualification Sync Job Status = "  +jobStatus);
		 sb.append("</div>");
	     sb.append("<div>Total Job Steps executed =" + dataMap.size());
	     sb.append("</div>");
		Iterator it = dataMap.entrySet().iterator();
		
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        
	        String [] values = (String[]) pair.getValue();
	        sb.append("<div>Step = [" + pair.getKey()+": Status= "+values[0]+ ": Message ="+values[1]+"]");
	        sb.append("</div>");
	    }
		return sb.toString();
	}
	


	
}

/**
 * 
 */
package com.softech.qualificationsync.executor.job.email;

/**
 * @author khurram.ahmed
 *
 */
public interface JobEmail {
	
	public boolean sendEmail();
	
	
}

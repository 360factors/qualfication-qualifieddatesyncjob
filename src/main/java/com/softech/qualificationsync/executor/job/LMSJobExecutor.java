/**
 * 
 */
package com.softech.qualificationsync.executor.job;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.softech.qualificationsync.service.webservice.WebServiceInvoker;
import com.softech.qualificationsync.service.webservice.model.RequestForTrainingPlanStatus;
import com.softech.qualificationsync.service.webservice.model.UserQualificationVO;

/**
 * @author khurram.ahmed
 *
 */
@Component
public class LMSJobExecutor {

	@Autowired
	private WebServiceInvoker webServiceInvoker;
	private JobResponse jobResponse = JobResponse.getInstance();
	/**
	 * 
	 * @throws Exception
	 */
	public void startLMSJob() throws Exception{
		List<RequestForTrainingPlanStatus []> requestForTrainingPlanStatus = getQuailificationData();
		try { 
			
			Map<String,Object> response = new HashMap<String,Object>();
			response.put("dataList", new ArrayList<Object>());
			
			for (RequestForTrainingPlanStatus[] request : requestForTrainingPlanStatus) {
				
				Map<String,Object> batchResponse = 
						webServiceInvoker.getQualificationStatusFromLMS(request);
				
				if (batchResponse != null && batchResponse.size() > 0  
						&& batchResponse.get("dataList") != null ) {
					
					((List<Object>)response.get("dataList")).addAll((List<Object>) batchResponse.get("dataList"));					
				}
			}		
			
			decorateJobResponseObject(response, JobStep.LMS_QUALFICATION_FETCH_STEP);
		}
		catch (Exception ex){
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			jobResponse.getJobStepMessage().put(JobStep.LMS_QUALFICATION_FETCH_STEP, sw.toString());
			jobResponse.getJobStepStatus().put(JobStep.LMS_QUALFICATION_FETCH_STEP, JobStatus.FAILED);
			throw ex;
		}
		
	}
	/**
	 * 
	 * @return
	 */
	private List<RequestForTrainingPlanStatus []> getQuailificationData(){
		
		List<RequestForTrainingPlanStatus[]> trainingPlanStatusRequests = 
				new ArrayList<RequestForTrainingPlanStatus[]>();
		
		JobResponse jobResponse = JobResponse.getInstance();
		UserQualificationVO [] userQualificationVOs = 
			(UserQualificationVO[]) jobResponse.getJobResponseMap().get(JobStep.PREDICT_QUALIFICATION_FETCH_STEP);
		
		List<RequestForTrainingPlanStatus> rqList = new ArrayList<>();
		int batchSize =1; 
		for (UserQualificationVO userQualificationVO : userQualificationVOs){
			
			RequestForTrainingPlanStatus requestForTrainingPlanStatus = new RequestForTrainingPlanStatus();			
			requestForTrainingPlanStatus.setVu360UserGuid(userQualificationVO.getVu360UserGuid());
			requestForTrainingPlanStatus.setTrainingPlan_Ids(userQualificationVO.getQualificationIds());
			rqList.add(requestForTrainingPlanStatus);
			
			if (batchSize%10==0) {
				trainingPlanStatusRequests.add(
						rqList.toArray(new RequestForTrainingPlanStatus[10]));
				rqList = new ArrayList<>();
			}
			if (batchSize == userQualificationVOs.length) {
				trainingPlanStatusRequests.add(
						rqList.toArray(new RequestForTrainingPlanStatus[batchSize%10]));
			}
			batchSize++;
		}
		
		
		return trainingPlanStatusRequests;

	}
	
	private void decorateJobResponseObject(	Map<String,Object> responseMap,JobStep jobStep){
		
		if (responseMap.size() > 0 ){
			List<?> jobList = (List<?>) responseMap.get("dataList");
			jobResponse.getJobResponseMap().put(jobStep,responseMap.get("dataList"));
			jobResponse.getJobStepMessage().put(jobStep, jobList.size()>0?"true":"false"); 
		}
		else{
			jobResponse.getJobStepMessage().put(jobStep, "false"); 
		}
		jobResponse.getJobStepStatus().put(jobStep, JobStatus.SUCCESS);
		
	}
}

/**
 * 
 */
package com.softech.qualificationsync.executor.job;

/**
 * @author khurram.ahmed
 *
 */
public enum JobStatus {
	
	SUCCESS,
	FAILED;
}

/**
 * 
 */
package com.softech.qualificationsync.executor.job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author khurram.ahmed
 *
 */
public class JobResponse {

	private Map<JobStep,Object> jobResponseMap = null;
	private static JobResponse jobResponse = null;
	private Map<JobStep,JobStatus> jobStepStatus = null;
	private Map<JobStep,String> jobStepMessage = null;
	private boolean isnextStepApplicable=false;
	
	private JobResponse() {
	
	}

	/**
	 * @return the jobResponseMap
	 */
	protected Map<JobStep, Object> getJobResponseMap() {
		if (null == this.jobResponseMap){
			jobResponseMap = new HashMap<JobStep, Object>();
		}
		return jobResponseMap;
	}

	/**
	 * @param jobResponseMap the jobResponseMap to set
	 */
	protected void setJobResponseMap(Map<JobStep, Object> jobResponseMap) {
		this.jobResponseMap = jobResponseMap;
	}
	
	public static JobResponse getInstance(){
		if (null ==  jobResponse){
			jobResponse = new JobResponse();
		}
		
		return jobResponse;
	}

	/**
	 * @return the overAllJobStatus
	 */
	protected Map<JobStep, JobStatus> getJobStepStatus() {
		if (null==jobStepStatus){
			jobStepStatus = new HashMap<JobStep,JobStatus>();
		}
		return jobStepStatus;
	}

	/**
	 * @param overAllJobStatus the overAllJobStatus to set
	 */
	protected void setJobStepStatus(Map<JobStep, JobStatus> jobStepStatus) {
		this.jobStepStatus = jobStepStatus;
	}
	
	
	
	public String getJobStatus(){
		List<JobStatus> jobStatuses= new ArrayList<JobStatus>(jobStepStatus.values());
		boolean isFailed= false;
		for (JobStatus jobStatus :  jobStatuses){
			if (jobStatus == JobStatus.FAILED){
				isFailed = true;
				break;
			}
		}
		return isFailed ?JobStatus.FAILED.name():JobStatus.SUCCESS.name();
	}
	/**
	 * @return the jobStepMessage
	 */
	protected Map<JobStep, String> getJobStepMessage() {
		if(null == jobStepMessage){
			jobStepMessage = new HashMap<JobStep,String>();
		}
		return jobStepMessage;
	}

	/**
	 * @param jobStepMessage the jobStepMessage to set
	 */
	protected void setJobStepMessage(Map<JobStep, String> jobStepMessage) {
		this.jobStepMessage = jobStepMessage;
	}
	
	public Map<String,String []> getJobStepMetaData(){
		Set<JobStep> jobSteps = jobStepStatus.keySet();
		List<JobStep> jobStepsList = Arrays.asList(jobSteps.toArray(new JobStep[jobSteps.size()]));
		
		List<JobStatus> jobStatuses = new ArrayList<>(jobStepStatus.values());
		Map<String, String []> jobStepData = new HashMap<>();
		int index = 0;
		int mapIndex = 0;
		for (JobStatus jobStatus :  jobStatuses){
			String [] jobdata = new String[2];
			jobdata[0] = jobStatus.name();
			String status =  jobStepMessage.get(jobStepsList.get(index));
			jobdata [1] = status.equalsIgnoreCase("true")?"Successfully executed the data":"No data is available for this job step.";
			jobStepData.put(jobStepsList.get(index).name(), jobdata);
			index++;
		}
		return jobStepData;
	}

	/**
	 * @return the isnextStepApplicable
	 */
	public boolean isIsnextStepApplicable() {
		return isnextStepApplicable;
	}

	/**
	 * @param isnextStepApplicable the isnextStepApplicable to set
	 */
	public void setIsnextStepApplicable(boolean isnextStepApplicable) {
		this.isnextStepApplicable = isnextStepApplicable;
	}
	
	
}

/**
 * 
 */
package com.softech.qualificationsync.executor.job;

/**
 * @author khurram.ahmed
 *
 */
public enum JobType {

	LMS_QUALIFICATION_STATUS_JOB,
	PREDICT360_QUALIFICATION_FETCH_JOB,
	PREDICT360_QUALIFICATION_UPDATE_JOB,
	EMAIL_JOB;
}

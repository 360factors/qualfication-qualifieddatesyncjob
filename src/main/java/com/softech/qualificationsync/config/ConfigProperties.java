/**
 * 
 */
package com.softech.qualificationsync.config;

/**
 * @author khurram.ahmed
 *
 */
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigProperties {

	private static final Properties PROPERTIES;
	private static final Logger LOGGER = Logger.getLogger(ConfigProperties.class);

	static {
		LOGGER.debug("loading config.properties");
		PROPERTIES = new Properties();
		InputStream inputStream = ConfigProperties.class.getClassLoader().getResourceAsStream("config.properties");
		try {
			//PROPERTIES.load(new FileInputStream("./notificationengine.properties"));
			try{
				PROPERTIES.load(inputStream);
			}catch(Exception e){
				LOGGER.error(e.getMessage());
				LOGGER.error("problem in loading 'config.properties' file from resource loader");
				try{
					PROPERTIES.load(new FileInputStream("./config.properties"));
				}catch(Exception exp){
					LOGGER.error("problem in loading 'config.properties' file from FileInput Stream", exp);
				}
			}
		} catch (Exception e) {
			LOGGER.error("problem in loading 'config.properties' file", e);
		}
	}

	public static Properties getProperties() {
		return PROPERTIES;
	}

}
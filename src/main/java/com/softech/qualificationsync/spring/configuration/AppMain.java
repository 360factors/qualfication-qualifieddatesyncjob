/**
 * 
 */
package com.softech.qualificationsync.spring.configuration;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.softech.qualificationsync.executor.job.JobExecutor;
import com.softech.qualificationsync.executor.job.JobExecutorImpl;
import com.softech.qualificationsync.executor.job.email.JobEmail;
import com.softech.qualificationsync.executor.job.email.JobEmailImpl;
import com.softech.qualificationsync.service.webservice.util.PropertiesScanner;

/**
 * @author khurram.ahmed
 * 
 */
public class AppMain {
	private static Logger logger = Logger.getLogger(AppMain.class);
	AbstractApplicationContext context = null;

	public static void main(String args[]) {
		
		logger.debug("Starting the Job sync main class");
		AppMain appMain = new AppMain();
		logger.debug("Loading the Spring Context");
		appMain.loadSpringContext();
		logger.debug("Spring context loaded successfully");
		appMain.context.getBean(PropertiesScanner.class);
		logger.debug("getting the JobExecutor maanaged bean from Spring context ");
		JobExecutor jobExecutor = appMain.context
				.getBean(JobExecutorImpl.class);
		try{
			logger.debug("Calling the JobExecutor starts job method");
			String status = jobExecutor.startJobs();
			JobEmail jobImpl = appMain.context
					.getBean(JobEmailImpl.class);
			boolean isEmailSend = jobImpl.sendEmail();
			if (isEmailSend){
				
			}
			//logger.debug("Qualification Job Sync process is completed with status Code : [" + status+"]");
			
		}
		finally {
			logger.debug("destroying the spring context");
			appMain.destroySpringContext();
			//logger.error("Error occurred : Shutting down the system process");
			System.exit(args.length);
			
		}

	}

	private void loadSpringContext() {
		context = new AnnotationConfigApplicationContext(SpringConfig.class);

	}

	private void destroySpringContext() {
		context.close();
		logger.debug("spring context closed successfully");
	}

}

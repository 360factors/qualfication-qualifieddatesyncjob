/**
 * 
 */
package com.softech.qualificationsync.spring.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author khurram.ahmed
 *
 */
@Configuration
@ComponentScan(basePackages="com.softech.qualificationsync")
public class SpringConfig {
	
	
	
}
